from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.serialization import Encoding
from django.conf import settings
from django.urls import re_path
from django.contrib import admin
from django.urls import include, path
from django_ca.views import CertificateRevocationListView

from django_pki_manager_api.django_pki_manager_api import urls as api_urls

urlpatterns = [
    re_path(r"", include("user_sessions.urls", "user_sessions")),
    path("admin/", admin.site.urls),
    re_path(r"^watchman/", include("watchman.urls")),
    path("ca/", include("django_ca.urls")),
    path("api/", include(api_urls)),
    path(
        "crl/<hex:serial>.pem",
        CertificateRevocationListView.as_view(
            type=Encoding.PEM,
            expires=86400,
        ),
        name="custom-crl",
    ),
]

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [
        path("__debug__/", include(debug_toolbar.urls)),
    ] + urlpatterns
